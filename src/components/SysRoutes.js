import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from '../pages/home/Home';
import Biography from '../pages/biography/Biography';
import Register from '../pages/register/Register';
import Contact from '../pages/contact/Contact';
import Favorites from '../pages/favorites/Favorites';
import Login from '../pages/Login';

const SysRoutes = () => {
    return (
        <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/biography" exact element={<Biography />} />
            <Route path="/registerBiography" exact element={<Register />} />
            <Route path="/contact" exact element={<Contact />} />
            <Route path="/favorites" exact element={<Favorites />} />
            <Route path="/login" exact element={<Login />} />
        </Routes>
    );
}

export default SysRoutes;