import React, { useContext, useState } from 'react'
import { BsHeartFill, BsHeart, BsPencil } from 'react-icons/bs';
import { FaCross, FaStar } from 'react-icons/fa';
import { BiTrash } from 'react-icons/bi';
import './BiographyCard.css';
import { UsersContext } from '../store/UsersProvider';
import dayjs from "dayjs";

function BiographyCard(props) {

  const ctxUser = useContext(UsersContext);

  function update() {
    ctxUser.updateFavorite(props.biography.id);
  }

  return (
    <div className='bio_card'>
      {
        props.biography.photoUrl.length == 0
          ? <div className='bio_empty_image' />
          : <img className='bio_card_image' src={props.biography.photoUrl} alt="Sem imagem" />
      }
      <div className="card_info">
        <div className="card_info_header">
          <h2 className='card_info_title'>{props.biography.name}</h2>
          {
            (props.showFavorite) && (
              props.isFavorite
                ? <BsHeartFill onClick={update} size={28} className="heart-filled" />
                : <BsHeart onClick={update} size={28} className="heart" />
            )
          }
        </div>

        <p><FaStar className='icon' />Data de Nascimento: {dayjs(props.biography.birthdate).format('DD/MM/YYYY')}</p>
        {props.biography.deathdate && <p><FaCross className='icon' />Data de Falecimento: {dayjs(props.biography.deathdate).format('DD/MM/YYYY')}</p>}
        <p>{props.biography.description}</p>
      </div>
    </div>
  );
}

export default BiographyCard
