import React from 'react'
import './Footer.css';
function Footer() {
  const year = new Date().getFullYear();

  return (
    <footer className="footer-container">
      <div className='footer-columns'>
        <div  className='column'>
          <label htmlFor="contato">Entre em contato conosco:</label>
          <div>E-mail: <a href="mailto:contato@exemplo.com">contato@exemplo.com</a></div>
          <div>Telefone: (19) 1234-5678</div>
        </div>
        <div  className='column'>
          <div>Página desenvolvida por Luiz Henrique Lessa</div>
          <div>{`Copyright © Luiz Henrique Lessa ADS ${year}`}</div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;







