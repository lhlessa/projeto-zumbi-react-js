import React, { useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './NavBar.css';
import { UsersContext } from '../store/UsersProvider';

function NavBar() {
  const ctxUser = useContext(UsersContext);
  const navigate = useNavigate()

  function logout() {
    ctxUser.logout();
    navigate('/');
  }
  return (
    <>
      <header className="header">
        <nav className="navbar">
          <ul>
            <li>
              
              <NavLink to="/">
              <img src="images/punho.png"
                className="fist-logo"
                alt="Portal Zumbi dos Palmares"
                title="Portal Zumbi dos Palmares"
                height="60">
              </img>
                Página inicial</NavLink>
            </li>
            <li>
              <NavLink to="/biography">Biografias</NavLink>
            </li>
            {ctxUser.authenticated &&
              <li>
                <NavLink to="/favorites">Favoritos</NavLink>
              </li>
            }
            {ctxUser.authenticated &&
              <li>
                <NavLink to="/registerBiography">Cadastro</NavLink>
              </li>
            }
            <li>
              <NavLink to="/contact">Fale Conosco</NavLink>
            </li>
            {
              !ctxUser.authenticated
                ?
                <li>
                  <NavLink to="/login">Entrar</NavLink>
                </li>
                : <li>
                  <div onClick={logout}>Sair</div>
                </li>
            }
          </ul>
        </nav>
      </header>
    </>
  );
}

export default NavBar;
