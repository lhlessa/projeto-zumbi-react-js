import React, { useState, useEffect, useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './NavBar2.css';
import { UsersContext } from '../store/UsersProvider';

function NavBar2() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const ctxUser = useContext(UsersContext);
  const navigate = useNavigate()

  function logout() {
    if (window.confirm("Deseja mesmo sair?")) {
      ctxUser.logout();
      navigate('/');
    }
  }

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <div className='navbar-logo'>
            <img src="images/punho.png"
              className="fist-logo"
              alt="Portal Zumbi dos Palmares"
              title="Portal Zumbi dos Palmares"
              height="60" />
          </div>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <NavLink to='/' className='nav-links' onClick={closeMobileMenu}>
                Página Inicial
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/biography'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Biografias
              </NavLink>
            </li>
            {ctxUser.authenticated &&
              <li className='nav-item'>
                <NavLink
                  to='/favorites'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Favoritos
                </NavLink>
              </li>
            }
            {(ctxUser.authenticated && ctxUser.user.isAdmin) &&
              <li className='nav-item'>
                <NavLink
                  to='/registerBiography'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Cadastro
                </NavLink>
              </li>
            }
            <li className='nav-item'>
              <NavLink
                to='/contact'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Contato
              </NavLink>
            </li>
            {
              !ctxUser.authenticated
                ?
                <li className='nav-item'>
                  <NavLink
                    to='/login'
                    className='nav-links'
                    onClick={closeMobileMenu}
                  >
                    Entrar
                  </NavLink>
                </li>
                : <li className="nav-item">
                  <div className='nav-links' onClick={logout}>Sair</div>
                </li>
            }
          </ul>
        </div>
      </nav>
    </>
  );
}

export default NavBar2;