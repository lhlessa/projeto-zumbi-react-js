import React, { useState, useEffect } from 'react';
import { db } from '../firebaseConfig';
import {
    collection,
    addDoc,
    getDocs,
} from 'firebase/firestore';

export const BiographiesContext = React.createContext();

const BiographiesProvider = (props) => {

    const [biographies, setBiographies] = useState([])
    const [searchText, setSearchText] = useState('');

    const fetchBiographies = async () =>{

        const biographiesAux = [];

        try{
            const queryBio = await getDocs(collection(db,'biographies'));
            queryBio.forEach((bio) => {
                biographiesAux.push({id: bio.id, ...bio.data()});
            });
            setBiographies(biographiesAux);
        }catch(error){
            console.error('Error fetching collection: ', error);
        }

    };

    useEffect(() => {
        fetchBiographies();
    }, []);

    const addBiography = async (bio) => {
        const newBiographies = [...biographies];
        const newBio = {
            name: bio.name,
            description: bio.description,
            birthdate: bio.birthdate,
            deathdate: bio.deathdate,
            photoUrl: bio.photoUrl,
        };
        
        try {
            const docRef = await addDoc(collection(db, "biographies"), newBio);
            newBiographies.push(newBio);
            fetchBiographies();
            return true;
        } catch (error) {
            return false;
        }
    };

    function getBiographies(){
        if (searchText.length > 0) {
            const lowerSearchText = searchText.toLowerCase();
            return biographies.filter(
                (bio) => bio.name.toLowerCase().includes(lowerSearchText) || bio.description.toLowerCase().includes(lowerSearchText));
            /*
            biographies == [
                bio,
                bio,
                bio,
            ]

            bio == {
                name: String,
                description: String,
                birthdate: Date,
                deathdate: Date,
                photoUrl: String,
                favorite: boolean,
            }
            */
        }
        return biographies;
    }

    return (
        <BiographiesContext.Provider value={{ biographies: biographies, addBiography: addBiography, getBiographies: getBiographies, searchText: searchText, setSearchText: setSearchText }}>
            {props.children}
        </BiographiesContext.Provider>
    );
}

export default BiographiesProvider;
