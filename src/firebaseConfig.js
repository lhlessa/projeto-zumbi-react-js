import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCoIYbvgJr3frd234HXaCeuSXwoijuk5ig",
  authDomain: "projeto-zumbi-dos-p.firebaseapp.com",
  projectId: "projeto-zumbi-dos-p",
  storageBucket: "projeto-zumbi-dos-p.appspot.com",
  messagingSenderId: "1060209060899",
  appId: "1:1060209060899:web:3f39f10336f3049a239596"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);

export { app, db, auth };