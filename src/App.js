
import './App.css';
import Navbar2 from './components/NavBar2';
import { BrowserRouter } from 'react-router-dom';
import './components/SysRoutes';
import SysRoutes from './components/SysRoutes';
import BiographiesProvider from './store/BiographiesProvider';
import UsersProvider from './store/UsersProvider';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <UsersProvider>
        <BrowserRouter>
          <Navbar2 />
          <BiographiesProvider>
            <SysRoutes />
          </BiographiesProvider>
          <Footer />
        </BrowserRouter>
      </UsersProvider>
    </div>
  );
}

export default App; 
