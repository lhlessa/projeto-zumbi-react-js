import React, { useContext } from 'react'
import {UsersContext} from '../store/UsersProvider';
import { useNavigate } from 'react-router-dom';
import './Login.css';

function Login() {
    
    const navigate = useNavigate();
    const ctxUser = useContext(UsersContext);
    function getEvent(event){
        event.preventDefault();
        const email = event.target.email.value;
        const password = event.target.password.value;

        ctxUser.authUser(email, password).then((authenticated) => {
            if (authenticated) {
                navigate('/')
            } else {
                alert('Usuário ou senha inválidos.')
            }
        })

    }
    return (
        <div className='login'>
            <form onSubmit={getEvent}>
                <label htmlFor='E-mail'>E-mail</label>
                <br/>
                <input type="text" id="email" placeholder="E-mail" /> <br />
                <label htmlFor='Senha'>Senha</label>
                <br/>
                <input type="password" id="password" placeholder="Senha" /> <br />
                <button>Entrar</button> <br />
            </form>
        </div>
    );
}

export default Login;
