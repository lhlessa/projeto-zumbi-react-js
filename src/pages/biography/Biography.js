import React, {useContext} from 'react';
import BiographyCard from '../../components/BiographyCard';
import { BiographiesContext } from '../../store/BiographiesProvider';
import { UsersContext } from '../../store/UsersProvider';
import './Biography.css'
import { AiFillCloseSquare } from 'react-icons/ai';

function Biography() {

    const BiographieCtx = useContext(BiographiesContext);
    const UserCtx = useContext(UsersContext);

    function clearSearchText() {
        BiographieCtx.setSearchText('');
    }

    return (
        <div className='bios'>
            {
                (BiographieCtx.searchText.length > 0) &&
                <div className='search_header'>
                    <label>Mostrando resultados que contêm: {BiographieCtx.searchText}</label>
                    <AiFillCloseSquare size={32} onClick={clearSearchText}/>
                </div>
            }
            {BiographieCtx.getBiographies().map((biography) => {
                return (
                    <BiographyCard 
                        key={biography.id+'bio'} 
                        biography={biography} 
                        showFavorite={UserCtx.authenticated && UserCtx.user != undefined} 
                        isFavorite={UserCtx.isFavorite(biography.id)}
                    />
                )
            }
                
            )}
        </div>
    );
}

export default Biography;