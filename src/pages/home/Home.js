import React, { useContext } from 'react';
import { BiographiesContext } from '../../store/BiographiesProvider';
import { useNavigate } from 'react-router-dom';
import { BsSearch } from 'react-icons/bs';
import './Home.css';

function Home() {

    let text = "";

    const ctxBio = useContext(BiographiesContext);
    const navigate = useNavigate();

    function search() {
        ctxBio.setSearchText(text);
        navigate('/biography');
    }

    const handleChange = event => {
        text = event.target.value;
    };

    return (
        <div className='container'>
            <img src="images/dandara.jpg" alt='Imagem Dandara' />
            <h1>Diploma Zumbi dos Palmares</h1>
            <div id="divBusca">
                <input type="text" id="txtBusca" onChange={handleChange} placeholder="Buscar uma biografia..." />
                <BsSearch size={32} onClick={search} />
            </div>
            <h2>Projeto Zumbi dos Palmares</h2>
            <p>O projeto Diploma Zumbi dos Palmares tem como objetivo valorizar a cultura negra e resgatar a história de Zumbi dos Palmares,
                líder do Quilombo dos Palmares. Através de biografias, conteúdos educacionais e uma abordagem inclusiva, buscamos disseminar
                conhecimento sobre a luta contra a escravidão e promover a igualdade racial. Todo ano a câmara municipal de Campinas homenageia alguém da comunidade
                negra com o diploma Zumbi dos Palmares. É uma homenagem para aquelas pessoas que
                contribuíram para o desenvolvimento da comunidade negra em Campinas.</p>
        </div>
    );
}

export default Home;