import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';
import './contact.css'

function Contact() {


    const form = useRef();

    const sendEmail = (event) => {
        event.preventDefault();

        emailjs.sendForm('service_8jmktq6', 'template_lo42jqj', form.current, 'G0Yhbr_gI4IwBI11G')
            .then((result) => {
                alert("E-mail enviado com sucesso")
            }, (error) => {
                alert("Erro ao enviar o E-mail!")
            });
    };
    return (
        <form className='container_contact' form ref={form} onSubmit={sendEmail}>
            <h2>Fale Conosco</h2>
            <label htmlFor="nome">Nome:</label>
            <input type="text" id="nome" name="nome" required />
            <label htmlFor="email">E-mail:</label>
            <input type="email" id="email" name="email" />
            <label htmlFor="telefone">Telefone:</label>
            <input type="tel" id="telefone" name="telefone" />
            <label htmlFor="mensagem">Mensagem:</label>
            <textarea id="mensagem" name="mensagem" required></textarea>
            <button type="submit">Enviar</button>
        </form>
    );

}
export default Contact;
