import React, {useState,useContext} from 'react';
import BiographyCard from '../../components/BiographyCard';
import { BiographiesContext } from '../../store/BiographiesProvider';
import { UsersContext } from '../../store/UsersProvider';
import './Favorites.css'

function Favorites() {

    const BiographieCtx = useContext(BiographiesContext);
    const UserCtx = useContext(UsersContext);

    return (
        <div className='favorites'>
            {BiographieCtx.biographies.map((biography) => {
                return (
                    UserCtx.isFavorite(biography.id) &&
                        <BiographyCard key={biography.id+'fav'} biography={biography} showFavorite={false} />
                        
                )
            }
                
            )}
        </div>
    );
}

export default Favorites;