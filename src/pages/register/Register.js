import React, { useContext } from 'react';
import { BiographiesContext } from '../../store/BiographiesProvider';
import './Register.css';

function Register() {

  const bioContext = useContext(BiographiesContext);

  function addRegister(event) {
    event.preventDefault();

    if (fieldValidation(event.target.nome.value, event.target.descricaoBiografia.value)) {

      const nome = event.target.nome.value;
      const descricao = event.target.descricaoBiografia.value;
      const dataNascimento = event.target.dataNascimento.value;
      const dataFalecimento = event.target.dataFalecimento.value;
      const urlImage = event.target.uploadPhoto.value;

      bioContext.addBiography({
        name: nome,
        description: descricao,
        birthdate: dataNascimento,
        deathdate: dataFalecimento,
        photoUrl: urlImage,
      }).then((added) => {
        if (added) {
          alert("Biografia adicionada com sucesso!");
          event.target.nome.value = '';
          event.target.descricaoBiografia.value = '';
          event.target.dataNascimento.value = '';
          event.target.dataFalecimento.value = '';
          event.target.uploadPhoto.value = '';
        } else {
          alert("Não foi possível realizar o cadastro da biografia.");
        }
      });
    }

  }

  function fieldValidation(nome, descricao) {

    if (nome.length > 100 || nome.length === 0) {
      alert("O campo nome não pode possuir mais de 100 caracteres ou ser vazio")
      return false;
    }

    if (descricao.length > 1000 || descricao.length === 0) {

      alert("O campo nome não pode possuir mais de 1000 caracteres ou ser vazio")
      return false;
    }

    return true;
  }

  return (
    <form onSubmit={addRegister}>
      <div className='container_register'>
        <h2>Cadastro de biografia</h2>
        <label htmlFor="nome">Nome*</label>
        <input type="text" name="nome" placeholder="Nome da personalidade" id="nome" required maxLength={100} />
        <label htmlFor="descricaoBiografia">Descrição*</label>
        <textarea id="descricaoBiografia" name="descricaoBiografia" placeholder="Descrição da biografia" required maxLength={1000}></textarea>
        <label htmlFor="dataNascimento">Data Nascimento*</label>
        <input type="date" name="dataNascimento" id="dataNascimento" required />
        <label htmlFor="dataFalecimento">Data Falecimento</label>
        <input type="date" name="dataFalecimento" id="dataFalecimento" />
        <label htmlFor="Carregar foto">Foto (URL)</label>
        <input type="text" name="uploadPhoto" placeholder="Url da foto biográfica" id="uploadPhoto" />
        <button id="cadastrar">Cadastrar</button>
      </div>
    </form>
  );
}

export default Register;
